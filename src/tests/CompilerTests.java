package tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

import main.Compiler;
import parser.ParseException;
import util.DuplicateIdentifierException;
import util.InvalidTypeException;
import util.UndeclaredIdentifierException;

public class CompilerTests {
	
	// This test function was designed to work with Unix like systems. 
	
	private void testCase(String expression, String result) 
			throws IOException, InterruptedException, ParseException, FileNotFoundException, UndeclaredIdentifierException, DuplicateIdentifierException, InvalidTypeException {
		
		Process p;
		
		p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "mkdir -p compiler/src compiler/bin"});
	    p.waitFor();
		p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "rm compiler/src/*.j compiler/src/*.class"});
	    p.waitFor();
		
	    System.out.println("Compiling to Jasmin source code");
		
	    Compiler.compile(expression);
		
	    System.out.println("Compiling to Jasmin bytecode");
		
		p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "java -jar jasmin.jar -d compiler/bin compiler/src/*.j"});
	    p.waitFor();	    
	    assertTrue("Compiled to Jasmin bytecode", p.exitValue() == 0);
		
	    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));
		
	    StringBuffer output = new StringBuffer();
        String line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + "\n");
        }
	    System.out.println(output.toString());
		
		p = Runtime.getRuntime().exec(new String[] {"sh","-c", "java -cp compiler/bin Main"});
	    p.waitFor();
		
	    reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
	    output = new StringBuffer();
        line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + "\n");
        }
	    System.out.println("Output: #"+output.toString()+"#");
	    
	    assertTrue(result.equals(output.toString()));
	}
	
	private void testCase(String expression, int value) throws Exception {
		try{
			testCase(expression, value+"\n");
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}		
	}
	
	@Test
	public void BasicTest() throws Exception {
		testCase("1\n", 1);
	}
	
	@Test
	public void testsLabClass02() throws Exception {
		testCase("1+2\n",3);
		testCase("1-2-3\n",-4);
		testCase("4*2\n",8);
		testCase("4/2/2\n",1);
		
		testCase("5 * (1 + 2)\n",15);
	}
	
	@Test
	public void testsLabClass05() throws Exception {
		testCase("decl x = decl y = 3 in 10*y end in x + decl y = 2 in x+y end end\n",62);
		testCase("decl x = 1 in decl x = 2 in x*5 end + x end\n",11);
		testCase("decl x = 1 y = 2 in decl x = 3 z = 4 in x + y + z end end\n",9);
	}
	
	@Test
	public void testsLabClass08() throws Exception {
		testCase("decl x = var(3) in x := decl y = 5 in *x + y end end\n",8);
		testCase("decl x = var(var(3)) in *x := decl y = 5 in **x + y end; **x end\n",8);
		testCase("decl x = var(5) in decl i = var(*x) in while *i > 0 do i := *i - 1; x := *x * 5 end; *x end end\n",15625);
		testCase("decl n = 5 in decl i = var(n) res = var(n) in while *i > 0 do i := *i - 1; if *i == 0 || *i == 1 then 1 else res := *res * *i end end; *res end end\n",120);
	}
	
	@Test
	public void testsLabClass10() throws Exception {
		testCase("(fun x:int => x end)(5)\n",5);
		testCase("decl f = fun g:(int)->int, x:int => g(g(x)) end in f(fun x:int => x * 2 end, 3) end\n",12);
		testCase("decl const = fun n:int => fun => n end end in const(5)() end\n", 5);
		testCase("decl f = fun v:ref(int) => v := *v + 1; v end x = var(10) in *f(x) end\n", 11);
		testCase("decl curry = fun f:(int, int)->int, x:int => fun y:int => f(x, y) end end in curry(fun a:int, b:int => a + b end, 2)(2) end\n", 4);
	}
	
	@Test
	public void testsProject() throws Exception {
		// recursive function
		testCase("declrec fact:(int)->int = fun n:int => if n == 0 then 1 else n * fact(n - 1) end end in fact(5) end\n",120);
		
		// simple object
		testCase("{i = 1}.i\n",1);
		// object inside object
		testCase("{o = {i = 1}}.o.i\n",1);
		// reference to object
		testCase("decl o = var({a = 1, b = 2}) in (*o).a + (*o).b end\n",3);
		// passing object to function
		testCase("decl f = fun o:ref({{a:int}}) => (*o).a * (*o).a end in f(var({a = 5})) end\n",25);
		// calling function inside object
		testCase("decl f = fun o:{{g:(int)->int}} => o.g(3) end in f({g = fun x:int => x * 2 end}) end\n",6);
		// returning object from function
		testCase("decl f = fun x:int => {a = x} end in f(3).a end\n",3);
		
		// recursive object declaration (with declrec)
		testCase("decl o = declrec this:{{a:ref(int), f:(int)->int, g:(int)->int}} = {a = var(4), f = fun x:int => this.a := *this.a - 1; if *this.a >= 0 then this.g(x * 2) else x end end, g = fun x:int => this.f(x - 1) end} in this end in o.f(2) end\n", 17);
	}
}





