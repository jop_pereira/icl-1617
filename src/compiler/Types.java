package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

public class Types {
	private static String dir;
	
	private static void dumpClass(String filename, ArrayList<String> code) throws FileNotFoundException{
		PrintStream out = new PrintStream(new FileOutputStream(dir + "/" + filename));
		
		for(String line : code)
			out.println(line);
		
		out.close();
	}
	/*
	private static void dumpFrameInterface() throws FileNotFoundException{
		ArrayList<String> code = new ArrayList<String>();
		
		code.add(".source frame.j");
		code.add(".interface public frame");
		code.add(".super java/lang/Object");
		code.add(".end method");
		
		dumpClass("frame.j", code);
	}
	*/
	private static void dumpRefClass(String classname, String type) throws FileNotFoundException{
		String filename = classname + ".j";
		ArrayList<String> code = new ArrayList<String>();
		
		code.add(".source " + filename);
		code.add(".class public " + classname);
		code.add(".super java/lang/Object");
		code.add(".field public v " + type);
		code.add("");
		code.add(".method public <init>()V");
		code.add("    aload_0");
		code.add("    invokespecial java/lang/Object/<init>()V");
		code.add("    return");
		code.add(".end method");
		
		dumpClass(filename, code);
	}
	
	public static void dump(String outDir) throws FileNotFoundException{
		dir = outDir;
		
		// not needed
		//dumpFrameInterface();
		
		dumpRefClass("ref_class", "Ljava/lang/Object;");
		dumpRefClass("ref_int", "I");
	}
}
