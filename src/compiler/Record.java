package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import types.*;
import util.*;

public class Record {
	int number;
	HashMap<String, IType> types;
	static int rnumber = 1;
	
	Record(RecordType rec) {
		this.number = rnumber++;
		
		this.types = new HashMap<String, IType>();
		
		for(Parameter p : rec.getParams()){
			this.types.put(p.getId(), p.getValue());
		}
		
	}
	
	public HashMap<String, IType> getTypes(){
		return types;
	}
	
	String classname(){
		return "record_" + this.number;
	}
	
	String filename(){
		return classname() + ".j";
	}
	
	String varname(String id){
		if(types.containsKey(id))
			return id;
		else
			return "";
	}
	
	String var(String id){
		return varname(id) + " " + types.get(id).getPrimitive();
	}
	
	
	@Override
	public boolean equals(Object other){
		if(other instanceof Record){
			HashMap<String, IType> otypes = ((Record) other).getTypes();
			
			return types.equals(otypes);
			
		}else if(other instanceof RecordType){
			ArrayList<Parameter> otypes = ((RecordType) other).getParams();
			
			if(types.size() != otypes.size())
				return false;
			
			for(int i = 0; i < otypes.size(); i++){
				if(!otypes.get(i).getValue().equals(types.get(otypes.get(i).getId())))
					return false;
			}
			
			return true;
			
		}else{
			return false;
		}
	}
	
	void dumpHeader(PrintStream out) {
		out.println(".source " + filename());
		out.println(".class " + classname());
		out.println(".super java/lang/Object");
	}
    
	void dumpDecls(PrintStream out) {
		// .field public <name> <type>
		for(HashMap.Entry<String, IType> e : types.entrySet())
			out.println(".field public " + var(e.getKey()));
	}
    
	void dumpFooter(PrintStream out) {
		out.println(".method public <init>()V");
		out.println("aload_0");
		out.println("invokespecial java/lang/Object/<init>()V");		
		out.println("return");
		out.println(".end method");
	}
	
	void dump(String dir) throws FileNotFoundException {
		PrintStream out = new PrintStream(new FileOutputStream(dir + "/" + filename()));
		
		dumpHeader(out);
		out.println("");
		dumpDecls(out);
		out.println("");
		dumpFooter(out);
		
		out.close();
	}
}
