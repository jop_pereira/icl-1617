package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import types.*;
import util.*;

public class CodeBlock {
	
	protected ArrayList<String> code;
	protected ArrayList<StackFrame> frames;
	private ArrayList<Function> functions;
	private ArrayList<FunInterface> ifuns;
	private ArrayList<Record> records;
	protected StackFrame frame;
	
	private String sp;
	private int lcounter;
    
	public CodeBlock() {
		code = new ArrayList<String>(100);
		frames = new ArrayList<StackFrame>();
		functions = new ArrayList<Function>();
		ifuns = new ArrayList<FunInterface>();
		records = new ArrayList<Record>();
		frame = null;
		
		sp = "1";
		lcounter = 1;
	}
	
	protected ArrayList<Function> getFunctions(){
		return functions;
	}
	
	protected ArrayList<FunInterface> getIfuns(){
		return ifuns;
	}
	
	protected ArrayList<Record> getRecords(){
		return records;
	}
	
	public String createLabel() {
		return "LBL_" + (lcounter++);
	}
	
	protected void setSP(int local){
		sp = Integer.toString(local);
	}
	
	
	public void emit_push(int n) {
		code.add("sipush " + n);
	}
	
	public void emit_add() {
		code.add("iadd");
	}
	
	public void emit_mul() {
		code.add("imul");
	}
	
	public void emit_div() {
		code.add("idiv");
	}
	
	public void emit_sub() {
		code.add("isub");
	}
    
	public void emit_neg() {
		code.add("ineg");
	}
	
	// compare
	private void emit_cmp_branch(String label_true, String label_false){
		this.emit_push(0);
		code.add("goto " + label_false);
		
		code.add(label_true + ":");
		this.emit_push(1);
		
		code.add(label_false + ":");
	}
	
	public void emit_eq() {
		String label_true = createLabel();
		String label_false = createLabel();
		
		code.add("if_icmpeq " + label_true);
		this.emit_cmp_branch(label_true, label_false);
	}
	
	public void emit_ne() {
		String label_true = createLabel();
		String label_false = createLabel();
		
		code.add("if_icmpne " + label_true);
		this.emit_cmp_branch(label_true, label_false);
	}
	
	public void emit_gt() {
		String label_true = createLabel();
		String label_false = createLabel();
		
		code.add("if_icmpgt " + label_true);
		this.emit_cmp_branch(label_true, label_false);
	}
	
	public void emit_ge() {
		String label_true = createLabel();
		String label_false = createLabel();
		
		code.add("if_icmpge " + label_true);
		this.emit_cmp_branch(label_true, label_false);
	}
	
	public void emit_lt() {
		String label_true = createLabel();
		String label_false = createLabel();
		
		code.add("if_icmplt " + label_true);
		this.emit_cmp_branch(label_true, label_false);
	}
	
	public void emit_le() {
		String label_true = createLabel();
		String label_false = createLabel();
		
		code.add("if_icmple " + label_true);
		this.emit_cmp_branch(label_true, label_false);
	}
	
	public void emit_not() {
		String label_true = createLabel();
		String label_false = createLabel();
    
		code.add("ifeq " + label_true);
		this.emit_cmp_branch(label_true, label_false);
	}
	// /compare
	
	// boolean operations
	public void emit_and() {
		String label_false1 = createLabel();
		String label_false2 = createLabel();
		String label_true = createLabel();
		
		code.add("ifeq " + label_false1);
		code.add("ifeq " + label_false2);
		
		// both are true
		this.emit_push(1);
		code.add("goto " + label_true);
		
		// first is false
		code.add(label_false1 + ":");
		code.add("pop");
		
		// second is false
		code.add(label_false2 + ":");
		this.emit_push(0);
		
		code.add(label_true + ":");
	}
    
	public void emit_or() {
		String label_true1 = createLabel();
		String label_true2 = createLabel();
		String label_false = createLabel();
		
		code.add("ifne " + label_true1);
		code.add("ifne " + label_true2);
		
		// both are false
		this.emit_push(0);
		code.add("goto " + label_false);
		
		// first is true
		code.add(label_true1 + ":");
		code.add("pop");
		
		// second is true
		code.add(label_true2 + ":");
		this.emit_push(1);
		
		code.add(label_false + ":");
	}
	// /boolean operations
	
	// frames
	public StackFrame createFrame(ArrayList<IType> types){
		ArrayList<String> classes = new ArrayList<String>();
		for(IType t : types)
			classes.add(t.getPrimitive());
		
		StackFrame fr = new StackFrame(classes, frame);
		
		code.add("new " + fr.classname());
		code.add("dup");
		code.add("invokespecial " + fr.classname() + "/<init>()V");
		if(fr.parentFrame != null){
			code.add("dup");
			code.add("aload " + sp);
			code.add("putfield " + fr.classname() + "/SL " + reftypeToJasmin(fr.parentFrame.classname()));
		}
		
		frames.add(fr);
		return fr;
	}

	public void pushFrame(StackFrame fr){
		frame = fr;
		code.add("astore " + sp);
	}
	
	public void popFrame(){
		if(frame != null){
			if(frame.parentFrame != null){
				code.add("aload " + sp);
				code.add("checkcast " + frame.classname());
				code.add("getfield " + frame.classname() + "/SL " + reftypeToJasmin(frame.parentFrame.classname()));
				code.add("astore " + sp);
				
				frame = frame.parentFrame;
			}else{
				code.add("aconst_null");
				code.add("astore " + sp);
			}
		}else{
			// throw ?
		}
	}
	
	public void emit_dup(){
		code.add("dup");
	}
	
	public void emit_storeFrame(StackFrame fr, int offset){
		code.add("putfield " + fr.classname() + "/" + fr.var(offset));
	}
	
	public void emit_loc(String id) throws UndeclaredIdentifierException {
		MemoryCell m = frame.find(id);
		
		StackFrame cur = frame;
		code.add("aload " + sp);
		code.add("checkcast " + cur.classname());
		for(int i = 0; i < m.getJumps(); i++){
			code.add("getfield " + cur.classname() + "/SL " + reftypeToJasmin(cur.parentFrame.classname()));
			cur = cur.parentFrame;
		}
		code.add("getfield " + cur.classname() + "/" + cur.var(m.getOffset()));
	}
	// /frames
    
    // references
    public void createRef(IType type){
    	String classname = toContainerClass(type);
		code.add("new " + classname);
		code.add("dup");
		code.add("invokespecial " + classname + "/<init>()V");
    }
    
    public void emit_putfield(IType type){
    	code.add("putfield " + toContainerClass(type) + "/v " + type.getPrimitive());
    }
    
    public void emit_getfield(IType type){
    	code.add("checkcast " + toContainerClass(type));
    	code.add("getfield " + toContainerClass(type) + "/v " + type.getPrimitive());
    }
    
    public void emit_castcont(IType type){
    	code.add("checkcast " + toContainerClass(type));
    }
    // /references
    
    // imperative
    public void emit_pop(){
    	code.add("pop");
    }
    
    public void emit_label(String label){
    	code.add(label + ":");
    }
    
    public void emit_jmp(String label){
    	code.add("goto " + label);
    }
    
    public void emit_if(String label){
    	code.add("ifeq " + label);
    }
    // /imperative
	
	// functions
	public Function createFunction(PFunType fun) throws DuplicateIdentifierException{
		return createFunction(fun, this);
	}
	
	protected Function createFunction(PFunType fun, CodeBlock parent) throws DuplicateIdentifierException{
		Function f = new Function(fun, frame, parent);
		FunInterface fi = addFunInterface(f);
		f.addInterface(fi);
		
		code.add("new " + f.classname());
		code.add("dup");
		code.add("invokespecial " + f.classname() + "/<init>()V");
		if(frame != null){
			code.add("dup");
			code.add("aload " + sp);
			code.add("putfield " + f.classname() + "/SL " + reftypeToJasmin(frame.classname()));
		}
		
		getFunctions().add(f);
		return f;
	}

	private FunInterface addFunInterface(Function f){
		for(FunInterface fi : getIfuns())
			if(fi.equals(f))
				return fi;
		
		FunInterface newfi = new FunInterface(f.getArgs(), f.getRet());
		getIfuns().add(newfi);
		return newfi;
	}
	
	private FunInterface addFunInterface(FunType f){
		for(FunInterface fi : getIfuns())
			if(fi.equals(f))
				return fi;
		
		FunInterface newfi = new FunInterface(f.getArgs(), f.getRet());
		getIfuns().add(newfi);
		return newfi;
	}
    
    public void emit_castfun(FunType fun){
    	String classname = addFunInterface(fun).classname();
    	code.add("checkcast " + classname);
    }
	
	public void emit_invoke(IType type){
		FunInterface fi = addFunInterface((FunType) type);
		code.add("invokeinterface " + fi.classname() + "/" + fi.methodSignature() + " " + (fi.getArgs().size() + 1));
	}
	// /functions
	
	// records
	public Record createRecord(RecordType rec){
		Record r = addRecord(rec);
		
		code.add("new " + r.classname());
		code.add("dup");
		code.add("invokespecial " + r.classname() + "/<init>()V");
		
		return r;
	}
	
	public Record addRecord(RecordType rec){
		for(Record r : getRecords())
			if(r.equals(rec))
				return r;
		
		Record r = new Record(rec);
		getRecords().add(r);
		return r;
	}
	
	public void emit_storeRecord(Record rec, String id){
		code.add("putfield " + rec.classname() + "/" + rec.var(id));
	}
	
	public void emit_access(RecordType t, String id) throws UndeclaredIdentifierException{
		Record rec = addRecord(t);
		
		code.add("checkcast " + rec.classname());
		code.add("getfield " + rec.classname() + "/" + rec.var(id));
	}
	// /records
	
    protected String reftypeToJasmin(String type) {
		return "L" + type + ";";
	}
	
    protected String toContainerClass(IType t){
		if(t instanceof IntType || t instanceof BoolType){
			return "ref_int";
		}else if(t instanceof RefType || t instanceof FunType || t instanceof RecordType){
			return "ref_class";
		}else{
			return "";
		}
	}
	
	void dumpHeader(PrintStream out) {
		out.println(".class public Main");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("    aload_0");
		out.println("    invokenonvirtual java/lang/Object/<init>()V");
		out.println("    return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("        ; set limits used by this method");
		out.println("        .limit locals 10");
		out.println("        .limit stack 256");
		out.println("");
		out.println("        ; setup local variables:");
		out.println("");
		out.println("        ;    1 - the PrintStream object held in java.lang.out");
		out.println("        getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("        ; place your bytecode here");
		out.println("        ; START");
		out.println("");
	}
    
	void dumpFooter(PrintStream out) {
		out.println("        ; END");
		out.println("");
		out.println("");		
		out.println("        ; convert to String;");
		out.println("        invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("        ; call println ");
		out.println("        invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");		
		out.println("        return");
		out.println("");		
		out.println(".end method");
	}
    
	void dumpCode(PrintStream out) {
		for( String s : code )
			out.println("        " + s);
	}
    
	public void dump(String dir) throws FileNotFoundException {
		// dump frames
		for(StackFrame f: frames) f.dump(dir);
		// dump functions
		for(Function f: functions) f.dump(dir);
		// dump types
		Types.dump(dir);
		
		// dump function interfaces
		for(FunInterface fi : ifuns) fi.dump(dir);
		
		// dump records
		for(Record rec : records) rec.dump(dir);
		
		// dump code
		PrintStream out = new PrintStream(new FileOutputStream(dir + "/Main.j"));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
		out.close();
	}
}