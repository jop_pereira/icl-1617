package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import types.FunType;
import types.IType;

public class FunInterface {
	private static int fcounter = 0;
	
	private int number;
	private ArrayList<IType> args;
	private IType ret;
	
	public FunInterface(ArrayList<IType> args, IType ret){
		this.number = fcounter++;
		
		this.args = args;
		this.ret = ret;
	}
	
	public ArrayList<IType> getArgs(){
		return args;
	}
	
	public IType getRet(){
		return ret;
	}
	
	public String methodSignature(){
		String args = "";
		for(IType a : this.args){
			args += a.getPrimitive();
		}
		
		return "call(" + args + ")" + this.ret.getPrimitive();
	}
	
	@Override
	public boolean equals(Object other){
		ArrayList<IType> oargs;
		IType oret;
		
		if(other instanceof FunInterface){
			FunInterface f = (FunInterface) other;
			
			oargs = f.getArgs();
			oret = f.getRet();
			
		}else if(other instanceof Function){
			Function f = (Function) other;
			oargs = f.getArgs();
			oret = f.getRet();
			
		}else if(other instanceof FunType){
			FunType f = (FunType) other;
			oargs = f.getArgs();
			oret = f.getRet();
			
		}else{
			return false;
		}
		
		return getArgs().equals(oargs) && getRet().equals(oret);
	}
	
	String classname(){
		return "type_" + this.number;
	}
	
	String filename(){
		return classname() + ".j";
	}
	
	public void dump(String dir) throws FileNotFoundException {
		PrintStream out = new PrintStream(new FileOutputStream(dir + "/" + filename()));
		out.println(".source " + filename());
		out.println(".interface public " + classname());
		out.println(".super java/lang/Object");
		out.println(".method public abstract " + methodSignature());
		out.println(".end method");
		out.close();
	}
	
}
