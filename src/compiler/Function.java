package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import types.*;
import util.*;

public class Function extends CodeBlock {
	
	private CodeBlock parent;
	
	private int number;
	private ArrayList<String> ids;
	private ArrayList<IType> args;
	private IType ret;
	private FunInterface fi;
	
	private StackFrame SL;
	
	private static int fcounter = 1;
	
	public Function(PFunType fun, StackFrame frame, CodeBlock parent) throws DuplicateIdentifierException{
		super();
		
		this.number = fcounter++;
		this.ids = fun.getIds();
		this.args = fun.getArgs();
		this.ret = fun.getRet();
		this.frame = frame;
		this.SL = frame;
		this.parent = parent;
		
		super.setSP(args.size() + 1);
		
		initFrame();
	}
	
	public ArrayList<IType> getArgs(){
		return args;
	}
	
	public IType getRet(){
		return ret;
	}
	
	public void addInterface(FunInterface fi){
		this.fi = fi;
	}
	
	String methodSignature(){
		String args = "";
		for(IType a : this.args){
			args += a.getPrimitive();
		}
		
		return "call(" + args + ")" + ret.getPrimitive();
	}
	
	String classname(){
		return "closure_" + this.number;
	}
	
	String filename(){
		return classname() + ".j";
	}
	
	
	// overrides
	@Override
	protected ArrayList<Function> getFunctions(){
		return parent.getFunctions();
	}
	@Override
	protected ArrayList<FunInterface> getIfuns(){
		return parent.getIfuns();
	}
	@Override
	protected ArrayList<Record> getRecords(){
		return parent.getRecords();
	}
	@Override
	public Function createFunction(PFunType fun) throws DuplicateIdentifierException{
		return createFunction(fun, parent);
	}
	// /overrides
	
	private void initFrame() throws DuplicateIdentifierException{
		ArrayList<String> classes = new ArrayList<String>();
		for(IType t : args)
			classes.add(t.getPrimitive());
		
		StackFrame newFrame = new StackFrame(classes, frame);
		
		code.add("new " + newFrame.classname());
		code.add("dup");
		code.add("invokespecial " + newFrame.classname() + "/<init>()V");
		if(newFrame.parentFrame != null){
			code.add("dup");
			code.add("aload 0");
			code.add("getfield " + classname() + "/SL L" + newFrame.parentFrame.classname() + ";");
			code.add("putfield " + newFrame.classname() + "/SL " + reftypeToJasmin(newFrame.parentFrame.classname()));
		}
		
		frames.add(newFrame);
		
		for(int i = 0; i < args.size(); i++){
			IType a = args.get(i);
			
			code.add("dup");
			if(a instanceof IntType || a instanceof BoolType){
				code.add("iload " + (i + 1));
			}else{
				code.add("aload " + (i + 1));
			}
			
			int offset = newFrame.assoc(ids.get(i));
			this.emit_storeFrame(newFrame, offset);
		}
		
		this.pushFrame(newFrame);
		code.add("; Code starts here");
	}
	
	@Override
	void dumpHeader(PrintStream out) {
		out.println(".source " + filename());
		out.println(".class public " + classname());
		out.println(".super java/lang/Object");
		out.println(".implements " + fi.classname());
		if(SL != null)
			out.println(".field public SL " + reftypeToJasmin(SL.classname()));
		out.println("");
		out.println(".method public <init>()V");
		out.println("    aload_0");
		out.println("    invokenonvirtual java/lang/Object/<init>()V");
		out.println("    return");
		out.println(".end method");
		out.println("");
		out.println(".method public " + methodSignature());
		out.println("        .limit locals " + (args.size() + 2));
		out.println("        .limit stack 256");
		out.println("");
	}

	@Override
	void dumpFooter(PrintStream out) {
		out.println("");
		if(ret instanceof IntType || ret instanceof BoolType){
			out.println("        ireturn");
		}else if(ret instanceof RefType || ret instanceof FunType || ret instanceof RecordType){
			out.println("        areturn");
		}else{
			out.println("        return ; invalid return type");
		}
		out.println(".end method");
	}
	
	@Override
	void dumpCode(PrintStream out) {
		for(String s : code)
			out.println("        " + s);
	}

	@Override
	public void dump(String dir) throws FileNotFoundException {
		// dump frames
		for(StackFrame f: frames) f.dump(dir);
		
		// dump code
		PrintStream out = new PrintStream(new FileOutputStream(dir + "/" + filename()));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
		out.close();
	}
}
