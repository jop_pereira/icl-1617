package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import util.DuplicateIdentifierException;
import util.UndeclaredIdentifierException;

public class StackFrame {
	int number;
	ArrayList<String> assocs;
	ArrayList<String> types;
	StackFrame parentFrame;
	static int fnumber = 1;
	
	StackFrame(ArrayList<String> types, StackFrame parentFrame) {
		this.number = fnumber++;
		this.types = types;
		this.parentFrame = parentFrame;
		
		this.assocs = new ArrayList<String>();
	}
	
	public MemoryCell find(String id) throws UndeclaredIdentifierException {
		int jumps = 0;
		
		StackFrame current = this;
		while(current != null) {
			int idx = current.assocs.indexOf(id);
			if(idx != -1)
				return new MemoryCell(jumps, idx);
			
			jumps++;
			current = current.parentFrame;
		}
		throw new UndeclaredIdentifierException(id);
	}
	public int assoc(String id) throws DuplicateIdentifierException {
        if(assocs.contains(id))
			throw new DuplicateIdentifierException(id);
		
		assocs.add(id);
		return assocs.size() - 1;
	}
	
	String classname(){
		return "frame_" + this.number;
	}
	
	String filename(){
		return classname() + ".j";
	}
	
	String varname(int idx){
		return "loc_" + idx;
	}
	
	String var(int idx){
		return varname(idx) + " " + types.get(idx);
	}
	
	void dumpHeader(PrintStream out) {
		out.println(".source " + filename());
		out.println(".class " + classname());
		out.println(".super java/lang/Object");
		//out.println(".implements frame");
		
		if(parentFrame != null){
			out.println("");
			out.println(".field public SL L" + parentFrame.classname() + ";");
		}
	}
    
	void dumpDecls(PrintStream out) {
		// .field public <name> <type>
		for(int i = 0; i < this.types.size(); i++)
			out.println(".field public " + var(i));
	}
    
	void dumpFooter(PrintStream out) {
		out.println(".method public <init>()V");
		out.println("aload_0");
		out.println("invokespecial java/lang/Object/<init>()V");		
		out.println("return");
		out.println(".end method");
	}
	
	void dump(String dir) throws FileNotFoundException {
		PrintStream out = new PrintStream(new FileOutputStream(dir + "/" + filename()));
		
		dumpHeader(out);
		out.println("");
		dumpDecls(out);
		out.println("");
		dumpFooter(out);
		
		out.close();
	}
}
