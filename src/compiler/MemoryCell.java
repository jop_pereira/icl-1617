package compiler;

public class MemoryCell {
	private int jumps;
	private int offset;
	
	public MemoryCell(int jumps, int offset){
		this.jumps = jumps;
		this.offset = offset;
	}
	
	public int getJumps(){
		return jumps;
	}
	
	public int getOffset(){
		return offset;
	}
}
