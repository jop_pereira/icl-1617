package values;

import java.util.ArrayList;

import util.Assoc;
import util.UndeclaredIdentifierException;

public class RecordValue implements IValue {
	
	private ArrayList<Assoc<IValue>> props;
	
	public RecordValue(ArrayList<Assoc<IValue>> props){
		this.props = props;
	}
	
	public ArrayList<Assoc<IValue>> getProps() {
		return props;
	}
	
	public IValue find(String id) throws UndeclaredIdentifierException{
		for(Assoc<IValue> assoc : props)
			if(id.equals(assoc.getId()))
				return assoc.getValue();
		
		throw new UndeclaredIdentifierException(id);
	}
	
	@Override
	public String toString(){
		String parameters = "";
		for(int i = 0; i < props.size(); i++){
			if(i > 0) parameters += ", ";
			parameters += props.get(i).getId() + ": " + props.get(i).getValue().toString();
		}
		return "{" + parameters + "}";
	}
}
