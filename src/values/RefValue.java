package values;

public class RefValue implements IValue {
	private IValue var;
	
	public RefValue(IValue var){
		this.var = var;
	}
	
	public void setValue(IValue var) {
		this.var = var;
	}
	
	public IValue getValue() {
		return this.var;
	}
	
	@Override
	public String toString(){
		return "ref(" + var.toString() + ")";
	}
}
