package values;

import java.util.ArrayList;

import ast.ASTNode;
import types.IType;
import util.Environment;
import util.Parameter;

public class FunValue implements IValue {
	
	private ArrayList<Parameter> params;
	private ASTNode body;
	private Environment<IValue> env;
	private IType ret;
	
	public FunValue(ArrayList<Parameter> params, ASTNode body, Environment<IValue> env){
		this.params = params;
		this.body = body;
		this.env = env;
		
		this.ret = body.getType(); // valid because typecheck is called
	}
	
	public ArrayList<Parameter> getParams() {
		return params;
	}

	public ASTNode getBody() {
		return body;
	}
	
	public Environment<IValue> getEnv() {
		return env;
	}
	
	@Override
	public String toString(){
		String parameters = "";
		for(int i = 0; i < params.size(); i++){
			if(i > 0) parameters += ", ";
			parameters += params.get(i).getValue().toString();
		}
		return "(" + parameters + ") -> " + (ret != null ? ret.toString() : "?");
	}
}
