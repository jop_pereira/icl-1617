package values;

public class BoolValue implements IValue {
	private boolean value;
	
	public BoolValue(boolean value){
		this.value = value;
	}
	
	public boolean getValue() {
		return value;
	}
	
	@Override
	public String toString(){
		return Boolean.toString(value);
	}
}
