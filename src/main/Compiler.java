package main;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import ast.ASTNode;
import compiler.CodeBlock;
import parser.ParseException;
import parser.Parser;
import types.IType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.InvalidTypeException;
import util.UndeclaredIdentifierException;

public class Compiler {

	public static void main(String args[]) throws ParseException {
		Parser parser = new Parser(System.in);
		ASTNode exp;
		
		try {
			exp = parser.Start();
			
			CodeBlock code = new CodeBlock();
			
			exp.typecheck(new Environment<IType>());
			exp.compile(code);
			
			code.dump("compiler/src");
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println ("Syntax Error!");
		}
	}
  	
	public static void compile(String s) throws ParseException, FileNotFoundException, UndeclaredIdentifierException, DuplicateIdentifierException, InvalidTypeException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		ASTNode n = parser.Start();
		
        CodeBlock code = new CodeBlock();
		n.typecheck(new Environment<IType>());
		n.compile(code);
		
		code.dump("compiler/src");
	}

}
