package main;

import java.io.ByteArrayInputStream;

import ast.ASTNode;
import parser.ParseException;
import parser.Parser;
import types.IType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.InvalidTypeException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class Console {

	public static void main(String args[]) {
		Parser parser = new Parser(System.in);

		while (true) {
			try {
				ASTNode n = parser.Start();
				
				n.typecheck(new Environment<IType>());
				Environment<IValue> env = new Environment<>();
				
				System.out.println("OK! - " + n.toString() + " = " + n.eval(env ));
			} catch (ParseException e) {
				System.out.println("Syntax Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (UndeclaredIdentifierException e) {
				System.out.println("Undeclared identifier " + e.getId() +"!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (DuplicateIdentifierException e) {
				System.out.println("Duplicated identifier " + e.getId() +"!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (ExecutionErrorException e) {
				System.out.println("Internal Error");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (InvalidTypeException e) {
				e.printStackTrace();
				System.out.println("Type error");
				parser.ReInit(System.in);
			}
		}
	}

	public static boolean accept(String s) throws ParseException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			parser.Start();
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static boolean acceptCompare(String s, int value) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			
			n.typecheck(new Environment<IType>());
			Environment<IValue> env = new Environment<>();
			return ((IntValue) n.eval(env)).getValue() == value;
		} catch (Exception e) {
			return false;
		}
	}

}
