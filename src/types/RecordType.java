package types;

import java.util.ArrayList;

import util.*;

public class RecordType implements IType {
	
	private ArrayList<Parameter> params;
	
	public RecordType(ArrayList<Parameter> params){
		this.params = params;
	}
	
	public ArrayList<Parameter> getParams(){
		return params;
	}
	
	public IType find(String id) throws UndeclaredIdentifierException{
		for(Parameter p : params)
			if(id.equals(p.getId()))
				return p.getValue();
		
		throw new UndeclaredIdentifierException(id);
	}
	
	@Override
	public String getPrimitive(){
		return "Ljava/lang/Object;";
	}
	
	
	@Override
	public boolean equals(Object other){
		if(other instanceof RecordType){
			RecordType o = (RecordType) other;
			ArrayList<Parameter> op = o.getParams();
			
			for(int i = 0; i < params.size(); i++){
				if(!params.get(i).getValue().equals(op.get(i).getValue())){
					return false;
				}
			}
			
		}else{
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString(){
		String parameters = "";
		for(int i = 0; i < params.size(); i++){
			if(i > 0) parameters += ", ";
			parameters += params.get(i).getId() + ":" + params.get(i).getValue().toString();
		}
		return "{{" + parameters + "}}";
	}
}
