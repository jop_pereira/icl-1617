package types;

public class IntType implements IType {
	public static IntType singleton = new IntType();

	@Override
	public boolean equals(Object other){
		return (other instanceof IntType);
	}
	
	public String getPrimitive(){
		return "I";
	}
	
	public String toString(){
		return "int";
	}
}
