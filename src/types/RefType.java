package types;

public class RefType implements IType {
	
	public IType var;
	
	public RefType(IType var){
		this.var = var;
	}
	
	public IType getValue() {
		return this.var;
	}
	
	public String getPrimitive(){
		return "Ljava/lang/Object;";
	}
	
	@Override
	public boolean equals(Object other){
		return (other instanceof RefType)
				&& this.getValue().equals(((RefType) other).getValue());
	}
	
	public String toString(){
		return "ref(" + var.toString() + ")";
	}
}
