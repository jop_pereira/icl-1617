package types;

import java.util.ArrayList;

public class FunType implements IType {
	protected ArrayList<IType> args;
	protected IType ret;
	
	public FunType(ArrayList<IType> args, IType ret){
		this.args = args;
		this.ret = ret;
	}
	
	public ArrayList<IType> getArgs(){
		return args;
	}
	
	public IType getRet(){
		return ret;
	}
	
	public String getPrimitive(){
		return "Ljava/lang/Object;";
	}
	
	@Override
	public boolean equals(Object other){
		if(other instanceof FunType){
			FunType o = (FunType) other;
			
			for(int i = 0; i < args.size(); i++){
				if(!args.get(i).equals(o.getArgs().get(i)))
					return false;
			}
			
			return args.equals(o.getArgs()) && ret.equals(o.getRet());
			
		}else{
			return false;
		}
	}
	
	@Override
	public String toString(){
		String parameters = "";
		for(int i = 0; i < args.size(); i++){
			if(i > 0) parameters += ", ";
			parameters += args.get(i).toString();
		}
		return "(" + parameters + ") -> " + ret.toString();
	}
}
