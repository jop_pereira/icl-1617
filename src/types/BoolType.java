package types;

public class BoolType implements IType {
	public static BoolType singleton = new BoolType();
	
	@Override
	public boolean equals(Object other){
		return (other instanceof BoolType);
	}
	
	public String getPrimitive(){
		return "I";
	}
	
	public String toString(){
		return "bool";
	}
}
