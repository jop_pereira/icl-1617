package types;

import java.util.ArrayList;

public class PFunType extends FunType {
	private ArrayList<String> ids;
	
	public PFunType(ArrayList<String> ids, ArrayList<IType> args, IType ret){
		super(args, ret);
		
		this.ids = ids;
	}
	
	public ArrayList<String> getIds(){
		return ids;
	}
}
