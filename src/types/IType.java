package types;

public interface IType {
	String getPrimitive();
}
