options {
    STATIC=false;
}

PARSER_BEGIN(Parser)

package parser;
import ast.*;
import types.*;
import util.*;
import java.util.ArrayList;

public class Parser {

}

PARSER_END(Parser)

SKIP :
{
  " "
| "\t"
| "\r"
}

TOKEN :
{
    < Num: ( "0" | ["1"-"9"] (["0"-"9"])* )  >
  | < TRUE: "true" >
  | < FALSE: "false" >
  
  | < PLUS : "+" >
  | < MINUS : "-">
  | < TIMES : "*">
  | < DIV : "/">
  | < LPAR : "(" >
  | < RPAR : ")" >
  
  | < GT: ">" >
  | < LT: "<" >
  | < GE: ">=" >
  | < LE: "<=" >
  | < EQ: "==" >
  | < NE: "!=" >
  
  | < AND: "&&" >
  | < OR: "||" >
  | < NOT: "!" >
  
  | < DECL : "decl" >
  | < DECLREC : "declrec" >
  | < ASSIGN : "=" >
  | < IN : "in" >
  | < END: "end" >
  
  | < WHILE: "while" > 
  | < DO: "do" >
  
  | < IF: "if" > 
  | < THEN: "then" > 
  | < ELSE: "else" > 
  
  | < ASSIGNMENT : ":=" >
  
  | < NL: ";" >
  | < COMMA: "," >
  
  | < REF: "var" > 
  //| < DEREF: "*" > 
  
  | < FUN : "fun" >
  | < COLON : ":" >
  | < ARROW : "=>" >
  
  | < LB : "{" >
  | < RB : "}" >
  | < DOT : "." >
  
  // types
  | < INT: "int" >
  | < BOOL: "bool" >
  | < REFT: "ref" >
  | < SARROW: "->" >
  
  | < Id: ["a"-"z","A"-"Z"] ( ["a"-"z","A"-"Z","0"-"9"] )* >
  | < EL: "\n" >
}

ASTNode Start():
{ ASTNode e; }
{
    e = Exp() <EL>
    { return e; }
}

ASTNode Exp():
{ ASTNode e1, e2; }
{
    e1 = Assign()
    (
        <NL> e2 = Assign() { e1 = new ASTSeq(e1, e2); }
    ) *
    { return e1; }
}

ASTNode Assign() :
{ ASTNode e1, e2; }
{
    e1 = Disj()
    (
        <ASSIGNMENT> e2 = Disj() { e1 = new ASTAssign(e1, e2); }
    ) *
    { return e1; }
}

ASTNode Disj() :
{ ASTNode e1, e2; }
{
    e1 = Conj()
    (
        <OR> e2 = Conj() { e1 = new ASTOr(e1, e2); }
    ) *
    { return e1; }
}

ASTNode Conj() :
{ ASTNode e1, e2; }
{
    e1 = Comp()
    (
        <AND> e2 = Comp() { e1 = new ASTAnd(e1, e2); }
    ) *
    { return e1; }
}

ASTNode Comp() :
{ ASTNode e1, e2; }
{
    e1 = Equality()
    (
        <GT> e2 = Equality() { e1 = new ASTGT(e1, e2); }
      | <GE> e2 = Equality() { e1 = new ASTGE(e1, e2); }
      | <LT> e2 = Equality() { e1 = new ASTLT(e1, e2); }
      | <LE> e2 = Equality() { e1 = new ASTLE(e1, e2); }
    ) *
    { return e1; }
}

ASTNode Equality() :
{ ASTNode e1, e2; }
{
    e1 = Arithm()
    (
        <EQ> e2 = Arithm() { e1 = new ASTEQ(e1, e2); }
      | <NE> e2 = Arithm() { e1 = new ASTNE(e1, e2); }
    ) *
    { return e1; }
}


ASTNode Arithm() :
{ ASTNode e1, e2; }
{
    e1 = Term() 
    (
        <PLUS>  e2 = Term() { e1 = new ASTAdd(e1,e2); }
      | <MINUS> e2 = Term() { e1 = new ASTSub(e1,e2); }
    )*
    { return e1; }
}

ASTNode Term() :
{ ASTNode e1, e2; }
{
    e1 = Unary()
    (
        <TIMES> e2 = Unary() { e1 = new ASTMul(e1,e2); }
      | <DIV>   e2 = Unary() { e1 = new ASTDiv(e1,e2); }
    )*
    { return e1; }
}

ASTNode Unary() :
{ ASTNode e; }
{
    e = Primary() { return e; }
  | <MINUS> e = Unary() { return new ASTNeg(e); }
  | <NOT> e = Unary() { return new ASTNot(e); }
  | <TIMES> e = Unary() { return new ASTDeref(e); }
}
/*
ASTNode Call() :
{ ASTNode e; ArrayList<ASTNode> args; }
{
    e = Access()
    (
        <LPAR> args = ArgumentList() <RPAR> {  e = new ASTCall(e, args); }
    )*
    { return e; }
}

ASTNode Access() :
{ ASTNode e; Token x; }
{
    e = Value()
    (
        <DOT> x = <Id> { e = new ASTAccess(e, x.image); }
    )*
    { return e; }
}
*/

ASTNode Primary() :
{ ASTNode e; ArrayList<ASTNode> args; Token x; }
{
    e = Value()
    (
        <LPAR> args = ArgumentList() <RPAR> {  e = new ASTCall(e, args); }
      | <DOT> x = <Id> { e = new ASTAccess(e, x.image); }
    )*
    { return e; }

}

ASTNode Value() :
{
  Token x;
  ASTNode e1, e2, e3;
  IType t;
  ArrayList<Binding> bindings;
  ArrayList<Parameter> parameters;
  ArrayList<TypedBinding> tb;
}
{
    x = <Id>  { return new ASTId(x.image); }
  // numbers
  | x = <Num> { return new ASTNum(Integer.parseInt(x.image)); }
  // booleans
  | <TRUE>  { return new ASTTrue(); }
  | <FALSE> { return new ASTFalse(); }
  // references
  | <REF> <LPAR> e1 = Exp() <RPAR> { return new ASTRef(e1); }
  // functions
  | <FUN> parameters = ParameterList() <ARROW> e1 = Exp() <END> { return new ASTFun(parameters, e1); }
  // objects
  | <LB> bindings = Fields() <RB> { return new ASTRecord(bindings); }
  
  | <LPAR>  e1 = Exp() <RPAR> { return e1; }
  
  // imperative constructs (or not)
  | <DECL> { bindings = new ArrayList<Binding>(); }
  	        (x = <Id> <ASSIGN> e1 = Exp() { bindings.add(new Binding(x.image, e1)); })+ <IN> e2 = Exp() <END>
            { return new ASTDecl(bindings, e2); }
  
  | <DECLREC> { tb = new ArrayList<TypedBinding>(); }
  	        (x = <Id> <COLON> t = Type() <ASSIGN> e1 = Exp() { tb.add(new TypedBinding(x.image, e1, t)); })+
  	        <IN> e2 = Exp() <END> { return new ASTDeclRec(tb, e2); }
  
  | <WHILE> e1 = Exp() <DO> e2 = Exp() <END> { return new ASTWhile(e1, e2); }
  | <IF> e1 = Exp() <THEN> e2 = Exp() <ELSE> e3 = Exp() <END> { return new ASTIf(e1, e2, e3); }
}



// typemap
ArrayList<Parameter> ParameterList() :
{ ArrayList<Parameter> params; Token x; IType t; }
{
    { params = new ArrayList<Parameter>(); }
    (        x = <Id> <COLON> t = Type() { params.add(new Parameter(x.image, t)); }
        (
            <COMMA> x = <Id> <COLON> t = Type() { params.add(new Parameter(x.image, t)); }        )*
    )?
    { return params; }
}
// types
ArrayList<ASTNode> ArgumentList() :
{ ArrayList<ASTNode> args; ASTNode e; }
{
    { args = new ArrayList<ASTNode>(); }
    (        e = Exp() { args.add(e); }
        (
            <COMMA> e = Exp() { args.add(e); }
        )*
    )?
    { return args; }
}

IType Type() :
{ IType t; ArrayList<IType> argTypes; ArrayList<Parameter> params; }
{
    <INT> { return IntType.singleton; }
  | <BOOL> { return BoolType.singleton; }
  | <REFT> <LPAR> t = Type() <RPAR> { return new RefType(t); }
  
  | <LPAR> argTypes = TypeList() <RPAR> <SARROW> t = Type() { return new FunType(argTypes, t); }
  
  | <LB><LB> params = ParameterList() <RB><RB> { return new RecordType(params); }
  //| <SELF>
}

ArrayList<IType> TypeList() :
{ ArrayList<IType> types; IType t; }
{
    { types = new ArrayList<IType>(); }
    (
        t = Type() { types.add(t); }
        (
            <COMMA> t = Type() { types.add(t); }
        )*
    )?
    { return types; }
}

ArrayList<Binding> Fields() :
{ Token x; ASTNode e; ArrayList<Binding> bindings; }
{
    { bindings = new ArrayList<Binding>(); }
    (
        x = <Id> <ASSIGN> e = Exp() { bindings.add(new Binding(x.image, e)); }
        (
            <COMMA> x = <Id> <ASSIGN> e = Exp() { bindings.add(new Binding(x.image, e)); }
        )*
    )?
    { return bindings; }
}





