package ast;

import java.util.ArrayList;

import compiler.*;
import types.*;
import util.*;
import values.*;

public class ASTRecord implements ASTNode {
	
	ArrayList<Binding> bindings;
	IType type;
	
    public ASTRecord(ArrayList<Binding> bindings){
		this.bindings = bindings;
    }
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		ArrayList<Assoc<IValue>> props = new ArrayList<Assoc<IValue>>();
		for(Binding b : bindings){
			props.add(new Assoc<IValue>(b.getId(), b.getExpr().eval(env)));
		}
		
		return new RecordValue(props);
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		Record rec = code.createRecord((RecordType) getType());
		
		for(Binding decl : bindings){
			code.emit_dup();
			decl.getExpr().compile(code);
			
			code.emit_storeRecord(rec, decl.getId());
		}
		
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException {
		ArrayList<Parameter> props = new ArrayList<Parameter>();
		for(Binding b : bindings){
			props.add(new Parameter(b.getId(), b.getExpr().typecheck(env)));
		}
		
		type = new RecordType(props);
		return type;
	}
	
	@Override
	public IType getType() {
		return type;
	}
	
	public String toString(){
		String props = "";
		for(int i = 0; i < bindings.size(); i++){
			if(i > 0) props += ", ";
			Binding p = bindings.get(i);
			props += p.getId() + " = " + p.getExpr().toString();
		}
		
		return "{" + props + "}";
	}
}
