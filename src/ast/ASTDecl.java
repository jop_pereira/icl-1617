package ast;

import java.util.ArrayList;

import compiler.*;
import types.*;
import util.*;
import values.*;

public class ASTDecl implements ASTNode {
	
	ArrayList<Binding> decls;
	ASTNode expr;
	IType type;
	
    public ASTDecl(ArrayList<Binding> decls, ASTNode expr)
    {
		this.decls = decls;
		this.expr = expr;
    }
    
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		Environment<IValue> newEnv = env.beginScope();
		
		for(Binding decl : decls){
			IValue idValue = decl.getExpr().eval(env);
			newEnv.assoc(decl.getId(), idValue);
		}
		IValue result = expr.eval(newEnv);
		newEnv.endScope();
		
		return result;
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		ArrayList<IType> types = new ArrayList<IType>();
		for(Binding decl : decls){
			types.add(decl.getExpr().getType());
		}
		
        StackFrame frame = code.createFrame(types);
		
		for(Binding decl : decls){
			code.emit_dup();
			decl.getExpr().compile(code);
			
			int offset = frame.assoc(decl.getId());
			code.emit_storeFrame(frame, offset);
		}
		
		code.pushFrame(frame);
		expr.compile(code);
		
		code.popFrame();
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		Environment<IType> newEnv = env.beginScope();
		
		for(Binding b : decls){
			IType idType = b.getExpr().typecheck(env);
			newEnv.assoc(b.getId(), idType);
		}
		
		type = expr.typecheck(newEnv);
		newEnv.endScope();
		
		return type;
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
    public String toString() {
    		String s = "";
    		for(Binding decl : decls)
    			s += decl.getId() + " = " + decl.getExpr().toString() + " ";
			
    		return "decl " + s + "in " + expr.toString() + " end";
    }
}

