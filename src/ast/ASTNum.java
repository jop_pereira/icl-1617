package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTNum implements ASTNode {
	
	int val;
	
	public ASTNum(int n) {
		val = n;
	}
	
	public IValue eval(Environment<IValue> env) throws InvalidTypeException {
		return new IntValue(val);
	}
	
	@Override
	public void compile(CodeBlock code) {
		code.emit_push(val);
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		return IntType.singleton;
	}
	
	public IType getType(){
		return IntType.singleton;
	}
	
	@Override
	public String toString() {
		return Integer.toString(val);
	}
}
