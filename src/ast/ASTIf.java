package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTIf implements ASTNode {
	
	ASTNode cond, body1, body2;
	IType type;
	
	public ASTIf(ASTNode c, ASTNode t, ASTNode e) {
		cond = c;
		body1 = t;
		body2 = e;
	}
	
	private boolean evalCond(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException{
		IValue c = cond.eval(env);
		return ((BoolValue) c).getValue();
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		if(evalCond(env)){
			return body1.eval(env);
		}else{
			return body2.eval(env);
		}
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		String label_true = code.createLabel();
		String label_false = code.createLabel();
		
		cond.compile(code);
		code.emit_if(label_false);
		body1.compile(code);
		
		code.emit_jmp(label_true);
		code.emit_label(label_false);
		body2.compile(code);
		
		code.emit_label(label_true);
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		IType c = cond.typecheck(env);
		IType b1 = body1.typecheck(env);
		IType b2 = body2.typecheck(env);
		
		if(c == BoolType.singleton && b1 == b2){
			type = b1;
			return type;
		}else{
			throw new InvalidTypeException();
		}
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
	public String toString() {
		return "if " + cond.toString() + " then " + body1.toString() + " else " + body2.toString() + " end";
	}
}
