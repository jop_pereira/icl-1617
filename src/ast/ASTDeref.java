package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTDeref implements ASTNode {
	
	ASTNode right;
	IType type;
	
	public ASTDeref(ASTNode r) {
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		IValue r = right.eval(env);
		
		return ((RefValue) r).getValue();
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		right.compile(code);
		code.emit_getfield(this.getType());
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		IType r = right.typecheck(env);
		
		if(r instanceof RefType){
			type = ((RefType) r).getValue();
			return type;
		}else{
			throw new InvalidTypeException();
		}
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
	public String toString() {
		return "*" + right.toString();
	}
}

