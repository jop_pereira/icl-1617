package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTOr implements ASTNode {
	
	ASTNode left, right;
	IType type;
	
	public ASTOr(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		BoolValue l = (BoolValue) left.eval(env);
		BoolValue r = (BoolValue)right.eval(env);
		
		boolean lv = l.getValue();
		boolean rv = r.getValue();
		
		return new BoolValue(lv || rv);
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code);
		right.compile(code);
		code.emit_or();
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if(l == BoolType.singleton && r == BoolType.singleton){
			type = BoolType.singleton;
			return type;
		}else{
			throw new InvalidTypeException();
		}
	}
	
	public IType getType(){
		return type;
	}

	@Override
	public String toString() {
		return left.toString() + " || " + right.toString();
	}
}
