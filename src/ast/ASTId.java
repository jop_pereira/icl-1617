package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTId implements ASTNode {
	
	String id;
	IType type;
	
	public ASTId(String id){
		this.id = id;
	}
	
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, ExecutionErrorException, InvalidTypeException {
		return env.find(id);
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		type = env.find(id);
		return type;
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException {
		code.emit_loc(id);
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
	public String toString() {
		return id;
	}
	
}

