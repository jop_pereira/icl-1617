package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTSeq implements ASTNode {
	
	ASTNode left, right;
	IType type;
	
	public ASTSeq(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		left.eval(env);
		return right.eval(env);
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code);
		code.emit_pop();
		right.compile(code);
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		left.typecheck(env);
		type = right.typecheck(env);
		
		return type;
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
	public String toString() {
		return left.toString() + "; " + right.toString();
	}
}
