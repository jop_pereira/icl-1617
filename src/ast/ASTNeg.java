package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTNeg implements ASTNode {
	
	ASTNode right;
	IType type;
	
	public ASTNeg(ASTNode r) {
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		IntValue r = (IntValue) right.eval(env);
		
		int rv = r.getValue();
		
		return new IntValue(-rv);
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		right.compile(code);
		code.emit_neg();
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		IType r = right.typecheck(env);
		
		if(r == IntType.singleton){
			type = IntType.singleton;
			return type;
		}else{
			throw new InvalidTypeException();
		}
	}
	
	public IType getType(){
		return type;
	}

	@Override
	public String toString() {
		return "-" + right.toString();
	}
}
