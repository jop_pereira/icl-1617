package ast;

import compiler.*;
import types.*;
import util.*;
import values.*;

public class ASTAccess implements ASTNode {
	
	ASTNode left;
	String label;
	
	IType type;
	
	public ASTAccess(ASTNode left, String label){
		this.left = left;
		this.label = label;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		RecordValue l = (RecordValue) left.eval(env);
		return l.find(label);
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code);
		code.emit_access((RecordType) left.getType(), label);
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException {
		IType l = left.typecheck(env);
		
		if(l instanceof RecordType){
			type = ((RecordType) l).find(label);
			return type;
		}else{
			throw new InvalidTypeException();
		}
	}
	
	@Override
	public IType getType() {
		return type;
	}
	
	public String toString(){
		return left.toString() + "." + label;
	}
}
