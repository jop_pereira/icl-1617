package ast;

import java.util.ArrayList;

import compiler.*;
import types.*;
import util.*;
import values.*;

public class ASTCall implements ASTNode {
	
	ASTNode left;
	ArrayList<ASTNode> args;
	
	IType type;
	
	public ASTCall(ASTNode l, ArrayList<ASTNode> args) {
		left = l;
		this.args = args;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		FunValue c = (FunValue) left.eval(env);
		ArrayList<Parameter> params = c.getParams();
		
		Environment<IValue> funenv = c.getEnv().beginScope();
		
		if(params.size() != args.size())
			throw new InvalidTypeException("Not calling function with same number of arguments as it was declared.");
		
		for(int i = 0; i < args.size(); i++){
			Parameter p = params.get(i);
			ASTNode a = args.get(i);
			IValue arg = a.eval(env);
			
			/* how to compare a value to a type
			if(!arg.equals(p.getType()))
				throw new InvalidTypeException("Calling function with argument of different type.");
			*/
			
			funenv.assoc(p.getId(), arg);
		}
		
		return c.getBody().eval(funenv);
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code);
		code.emit_castfun((FunType) left.getType());
		for(ASTNode a : args)
			a.compile(code);
		
		code.emit_invoke(left.getType());
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException {
		IType fun = left.typecheck(env);
		
		if(fun instanceof FunType){
			FunType f = (FunType) fun;
			
			for(int i = 0; i < args.size(); i++){
				IType a = args.get(i).typecheck(env);
				IType p = f.getArgs().get(i);
				
				if(!a.equals(p))
					throw new InvalidTypeException();
			}
			
			type = f.getRet();
			return type;
			
		}else{
			throw new InvalidTypeException();
		}
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
	public String toString() {
		String arguments = "";
		for(int i = 0; i < args.size(); i++){
			if(i > 0) arguments += ", ";
			arguments += args.get(i).toString();
		}
		
		return left.toString() + "(" + arguments + ")";
	}
}
