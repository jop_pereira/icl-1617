package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTNE implements ASTNode {
	
	ASTNode left, right;
	IType type;
	
	public ASTNE(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		IValue l = left.eval(env);
		IValue r = right.eval(env);
		
		if(l instanceof IntValue && r instanceof IntValue){
			int lv = ((IntValue) l).getValue();
			int rv = ((IntValue) r).getValue();
			
			return new BoolValue(lv != rv);
			
		}else if(l instanceof BoolValue && r instanceof BoolValue){
			boolean lv = ((BoolValue) l).getValue();
			boolean rv = ((BoolValue) r).getValue();
			
			return new BoolValue(lv != rv);
		}
		
		throw new InvalidTypeException();
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code);
		right.compile(code);
		code.emit_ne();
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if(l == IntType.singleton && r == IntType.singleton
			|| l == BoolType.singleton && r == BoolType.singleton){
			type = BoolType.singleton;
			return type;
		}else{
			throw new InvalidTypeException();
		}
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
	public String toString() {
		return left.toString() + " != " + right.toString();
	}
}
