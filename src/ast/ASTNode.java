package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public interface ASTNode {
	
	IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException; 
	
	void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException;
	
	IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException;
	
	IType getType();
}

