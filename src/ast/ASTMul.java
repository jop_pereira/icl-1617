package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTMul implements ASTNode {
	
	ASTNode left, right;
	IType type;
	
	public ASTMul(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		IntValue l = (IntValue) left.eval(env);
		IntValue r = (IntValue) right.eval(env);

		int lv = l.getValue();
		int rv = r.getValue();
		
		return new IntValue(lv * rv);
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		this.left.compile(code);
		this.right.compile(code);
		code.emit_mul();		
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if(l == IntType.singleton && r == IntType.singleton){
			type = IntType.singleton;
			return type;
		}else{
			throw new InvalidTypeException();
		}
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
	public String toString() {
		return left.toString() + " * " + right.toString();
	}
}
