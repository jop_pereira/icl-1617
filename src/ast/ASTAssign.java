package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTAssign implements ASTNode {
	
	ASTNode left, right;
	IType type;
	
	public ASTAssign(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		IValue l = left.eval(env);
		IValue r = right.eval(env);
		
		((RefValue) l).setValue(r);
		
		return r;
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code);
		code.emit_dup();
		code.emit_castcont(right.getType());
		right.compile(code);
		code.emit_putfield(right.getType());
		code.emit_getfield(right.getType());
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if(l instanceof RefType){
			IType lt = ((RefType) l).getValue();
			if(lt.equals(r)){
				type = r;
				return type;
			}
		}
		
		throw new InvalidTypeException();
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
	public String toString() {
		return left.toString() + " := " + right.toString();
	}
}
