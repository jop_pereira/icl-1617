package ast;

import java.util.ArrayList;

import compiler.*;
import types.*;
import util.*;
import values.*;

public class ASTDeclRec implements ASTNode {
	
	ArrayList<TypedBinding> decls;
	ASTNode expr;
	IType type;
	
    public ASTDeclRec(ArrayList<TypedBinding> decls, ASTNode expr){
		this.decls = decls; 
		this.expr = expr;
    }
    
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		Environment<IValue> newEnv = env.beginScope();
		
		for(TypedBinding decl : decls)
			newEnv.assoc(decl.getId(), null);
		
		for(TypedBinding decl : decls){
			IValue idValue = decl.getExpr().eval(newEnv);
			newEnv.update(decl.getId(), idValue);
		}
		IValue result = expr.eval(newEnv);
		newEnv.endScope();
		
		return result;
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		ArrayList<IType> types = new ArrayList<IType>();
		for(TypedBinding decl : decls){
			types.add(decl.getExpr().getType());
		}
		
        StackFrame frame = code.createFrame(types);
        code.emit_dup();
		code.pushFrame(frame);
		
		for(TypedBinding b : decls){
			frame.assoc(b.getId());
		}
		
		for(TypedBinding b : decls){
			code.emit_dup();
			b.getExpr().compile(code);
			code.emit_storeFrame(frame, frame.find(b.getId()).getOffset());
		}
		code.emit_pop();
		
		expr.compile(code);
		
		code.popFrame();
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		Environment<IType> newEnv = env.beginScope();
		
		for(TypedBinding b : decls){
			newEnv.assoc(b.getId(), b.getType());
		}
		
		for(TypedBinding b : decls){
			IType idType = b.getExpr().typecheck(newEnv);
			if(!idType.equals(b.getType()))
				throw new InvalidTypeException();
			newEnv.update(b.getId(), idType);
		}
		
		type = expr.typecheck(newEnv);
		newEnv.endScope();
		
		return type;
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
    public String toString() {
    		String s = "";
    		for(TypedBinding decl : decls)
    			s += decl.getId() + ":" + decl.getType().toString() + " = " + decl.getExpr().toString() + " ";
			
    		return "declrec " + s + "in " + expr.toString() + " end";
    }
}

