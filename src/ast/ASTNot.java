package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTNot implements ASTNode {
	
	ASTNode right;
	IType type;
	
	public ASTNot(ASTNode r) {
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		BoolValue r = (BoolValue) right.eval(env);
		
		boolean rv = r.getValue();
		
		return new BoolValue(!rv);
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		right.compile(code);
		code.emit_not();
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		IType r = right.typecheck(env);
		
		if(r == BoolType.singleton){
			type = BoolType.singleton;
			return type;
		}else{
			throw new InvalidTypeException();
		}
	}
	
	public IType getType(){
		return type;
	}

	@Override
	public String toString() {
		return "!" + right.toString();
	}
}
