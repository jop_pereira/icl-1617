package ast;

import java.util.ArrayList;

import compiler.*;
import types.*;
import util.*;
import values.*;

public class ASTFun implements ASTNode {
	
	ArrayList<Parameter> params;
	ASTNode body;
	
	PFunType type;
	
	public ASTFun(ArrayList<Parameter> params, ASTNode body) {
		this.params = params;
		this.body = body;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return new FunValue(params, body, env);
	}
	
	@Override
	public void compile(CodeBlock code) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		Function f = code.createFunction(type);
		body.compile(f);
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException {
		ArrayList<String> ids = new ArrayList<String>();
		ArrayList<IType> args = new ArrayList<IType>();
		Environment<IType> funenv = env.beginScope();
		
		for(Parameter p : params){
			ids.add(p.getId());
			args.add(p.getValue());
			funenv.assoc(p.getId(), p.getValue());
		}
		
		IType ret = body.typecheck(funenv);
		type = new PFunType(ids, args, ret);
		return type;
	}
	
	public IType getType(){
		return type;
	}
	
	@Override
	public String toString() {
		String parameters = "";
		for(int i = 0; i < params.size(); i++){
			if(i > 0) parameters += ", ";
			Parameter p = params.get(i);
			parameters += p.getId() + ":" + p.getValue().toString();
		}
		return "fun " + parameters + " => " + body.toString() + " end";
	}
}
