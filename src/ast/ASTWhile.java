package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTWhile implements ASTNode {
	
	ASTNode cond, body;
	
	public ASTWhile(ASTNode c, ASTNode b) {
		cond = c;
		body = b;
	}
	
	private boolean evalCond(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException{
		IValue c = cond.eval(env);
		return ((BoolValue) c).getValue();
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		// iterative
		//while(evalCond(env)){
		//	body.eval(env);
		//}
		//
		//return new BoolValue(false);
		
		// recursive
		if(evalCond(env)){
			body.eval(env);
			return this.eval(env);
		}else{
			return new BoolValue(false);
		}
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		String begin = code.createLabel();
		String end = code.createLabel();
		
		code.emit_label(begin);
		cond.compile(code);
		code.emit_if(end);
		
		body.compile(code);
		code.emit_pop();
		code.emit_jmp(begin);
		
		code.emit_label(end);
		code.emit_push(0);
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		// return bool
		IType c = cond.typecheck(env);
		body.typecheck(env);
		
		if(c == BoolType.singleton)
			return BoolType.singleton;
		else
			throw new InvalidTypeException();
	}
	
	public IType getType(){
		return BoolType.singleton;
	}
	
	@Override
	public String toString() {
		return "while " + cond.toString() + " do " + body.toString() + " end";
	}
}
