package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTRef implements ASTNode {
	
	ASTNode right;
	IType type;
	
	public ASTRef(ASTNode r) {
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		IValue r = right.eval(env);
		
		return new RefValue(r);
	}
	
	@Override
	public void compile(CodeBlock code) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		code.createRef(right.getType());
		code.emit_dup();
		right.compile(code);
		code.emit_putfield(right.getType());
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		IType r = right.typecheck(env);
		type = new RefType(r);
		return type;
	}
	
	public IType getType(){
		return type;
	}

	@Override
	public String toString() {
		return "var(" + right.toString() + ")";
	}
}

