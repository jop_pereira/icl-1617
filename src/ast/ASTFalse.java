package ast;

import compiler.*;
import types.*;
import values.*;
import util.*;

public class ASTFalse implements ASTNode {
	
	public ASTFalse() {
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, InvalidTypeException {
		return new BoolValue(false);
	}

	@Override
	public void compile(CodeBlock code) {
		code.emit_push(0);
	}

	@Override
	public IType typecheck(Environment<IType> env) throws UndeclaredIdentifierException, InvalidTypeException, DuplicateIdentifierException{
		return BoolType.singleton;
	}
	
	public IType getType(){
		return BoolType.singleton;
	}
	
	@Override
	public String toString() {
		return "false";
	}
}
