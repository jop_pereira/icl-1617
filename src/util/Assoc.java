package util;

public class Assoc<T> {
	String id;
	T value;
	
	public Assoc(String id, T value) {
		this.id = id;
		this.value = value;
	}
	
	public String getId(){
		return id;
	}
	
	public T getValue(){
		return value;
	}
	
	@Override
	public String toString(){
		return "(" + id + ", " + value.toString() + ")";
	}
}