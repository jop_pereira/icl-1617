package util;

import ast.*;
import types.*;

public class TypedBinding extends Binding {
	
	private IType type;
	
	public TypedBinding(String id, ASTNode expr, IType type) {
		super(id, expr);
		
		this.type = type;
	}
	
	public IType getType(){
		return type;
	}
}
